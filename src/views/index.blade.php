@extends('layouts.wrapper', [
    'pageTitle' => 'Hello World! - Index'
])

@section('content')
    <h1>Hello World!</h1>
    <p>This is a brand new module that is external to CCPS Core.</p>
@endsection()

