<?php

namespace Uncgits\Ccps\Helloworld;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // register Artisan commands, define publishing, define route-loading or migration-loading... etc.
        $this->loadViewsFrom(__DIR__ . '/views', 'helloworld');

        $this->loadMigrationsFrom(__DIR__ . '/migrations');
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
