<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Uncgits\Ccps\Helloworld\Seeders\PermissionsTableSeeder;
use App\CcpsCore\Permission;
use App\CcpsCore\Role;

class SeedHelloworldPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Artisan::call('db:seed', [
            '--class' => 'Uncgits\\Ccps\\Helloworld\\Seeders\\PermissionsTableSeeder',
            '--force' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // grab permissions to get rid of
        $permissionsSeeder = new PermissionsTableSeeder();
        $permissions = collect($permissionsSeeder->permissions)->pluck('name');

        $permissionIds = Permission::whereIn('name', $permissions)->get()->pluck('id');

        // detach these permissions from all roles
        $roles = Role::all()->each(function($item, $key) use ($permissionIds) {
            $item->detachPermissions($permissionIds);
        });

        $deletedRecords = Permission::wherein('id', $permissionIds)->delete();
    }
}
