<?php

namespace Uncgits\Ccps\Helloworld\Seeders;

use App\CcpsCore\Permission;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Uncgits\Ccps\Seeders\CcpsValidatedSeeder;

class PermissionsTableSeeder extends CcpsValidatedSeeder
{
    public $permissions = [
        // helloworld
        [
            "name" => "helloworld.view",
            "display_name" => "Hello World - View",
            "description" => "View the Hello World components"
        ],
        [
            "name" => "helloworld.edit",
            "display_name" => "Hello World - Edit",
            "description" => "Edit the Hello World components"
        ],
        [
            "name" => "helloworld.delete",
            "display_name" => "Hello World - Delete",
            "description" => "Delete the Hello World components"
        ],
        [
            "name" => "helloworld.create",
            "display_name" => "Hello World - Create",
            "description" => "Create new Hello World components"
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $writeConsoleOutput = App::runningInConsole();

        if ($writeConsoleOutput) {
            // get console output
            $output = $this->command->getOutput();
        }

        // validate
        try {
            $this->validateSeedData($this->permissions, $this->permissionArrayConstruction);
            $this->checkForExistingSeedData($this->permissions, Permission::all());

            $mergeData = [
                'source_package' => 'uncgits/ccps-module-helloworld',
                'created_at' => date("Y-m-d H:i:s", time()),
                'updated_at' => date("Y-m-d H:i:s", time()),
                'editable' => 0
            ];

            $this->commitSeedData($this->permissions, 'ccps_permissions', $mergeData);

        } catch (InvalidSeedDataException $e) {
            if ($writeConsoleOutput) {
                $output->error($e->getMessage());
                return;
            }
        }
    }
}
