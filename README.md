# CCPS Framework - Hello World Module

This is a basic Module (Hello World) for testing.

# Installation

1. `composer require uncgits/ccps-module-helloworld`
2. `php artisan migrate`
3. Place config below into `config/ccps.php` as described.
4. Assign new package permissions to whatever roles you want to have them.

# Config example

This should be placed in `app/config/ccps.php`, inside of the `modules` array:

```
'helloworld' => [
    'package' => 'uncgits/ccps-module-helloworld',
    'icon' => 'hand-paper-o',
    'title' => 'Hello World',
    'index' => 'helloworld',
    'parent' => 'admin',
    'required_permissions' => '*',
    'use_custom_routes' => false,
    'custom_view_path' => false,
],
```

# Questions?

- [ccps-developers-l@uncg.edu](mailto:ccps-developers-l@uncg.edu)
- [https://bitbucket.org/uncg-its/ccps-core](https://bitbucket.org/uncg-its/ccps-core)